#!/bin/bash

while true; do
  STATUSCODE=$(curl --max-time 1 -LI -o /dev/null -w '%{http_code}' -s -u $ENV_IN_USR:$ENV_IN_PWD $ENV_IN_SVC.$ENV_IN_NS.svc.cluster.local:5984/_membership)
  if [ $STATUSCODE = 200 ]
  then
    break
  fi

  echo $"status code: $STATUSCODE ...try again"
  sleep 5s
done

JSON=$(curl -s -u $ENV_IN_USR:$ENV_IN_PWD $ENV_IN_SVC.$ENV_IN_NS.svc.cluster.local:5984/_membership)
NODES=$(jq '.cluster_nodes[]' -r <<< $JSON)

for NODE in $NODES; do
  URL="$NODE:5984"
  echo $URL

  while true; do
    STATUS=$(curl -s -u $ENV_IN_USR:$ENV_IN_PWD "$URL/_up")
    STATUSVAL=$(jq '.status' -r <<< $STATUS)

    if [[ $STATUSVAL != ok ]]
    then
      echo $"$NODE: status=notReady"
      sleep 5s
    fi

    break
  done

  echo $"$NODE: try creating account"
  while true; do
    RESULT=$(curl -X PUT -s -u $ENV_IN_USR:$ENV_IN_PWD $URL/_node/$NODE/_config/admins/$ENV_IN_NUSR -d \"$ENV_IN_NPWD\")
    echo $RESULT

    if [[ $RESULT == \"\"* || $RESULT == \"-pbkdf2* ]]
    then
      echo $"$NODE: account created"
      break
    fi

    sleep 5s
  done
  echo "--------------------"
done

while true; do sleep 1d; done