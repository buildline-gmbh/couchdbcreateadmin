FROM alpine:latest

RUN apk update && \
    apk add --no-cache curl jq bash

WORKDIR "/application"
COPY ./scripts/* ./

RUN adduser \
  --uid 2808 \
  --disabled-password \
  --home /application \
  --gecos '' application \
  && chown -R application /application

USER application
ENTRYPOINT ["./couchdb_renew_pw.sh"]